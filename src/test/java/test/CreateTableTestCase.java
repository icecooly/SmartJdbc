package test;

import io.itit.smartjdbc.enums.ColumnType;
import io.itit.smartjdbc.model.Model;
import io.itit.smartjdbc.model.ModelColumn;
import io.itit.smartjdbc.model.ModelIndex;
import io.itit.smartjdbc.model.ModelPrimaryKey;
import io.itit.smartjdbc.provider.entity.EntityCreateTable;
import test.dao.BizDAO;

/**
 * 
 * @author skydu
 *
 */
public class CreateTableTestCase extends BaseTestCase{
	//
	static {
	    System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "debug");
	}
	//
	BizDAO dao;
	//
	public CreateTableTestCase() {
		dao=new BizDAO();
	}
	//
	public void testCreateTable() {
		Model model=new Model();
		model.tableName="t_xxxx";
		model.comment="t_xxxx";
		ModelColumn column=new ModelColumn();
		column.name="id";
		column.defaultValue=0;
		column.type=ColumnType.INT;
		column.notnull=true;
		column.comment="id";
		model.columnList.add(column);
		model.primaryKey=new ModelPrimaryKey();
		model.primaryKey.columnList.add("id");
		ModelIndex index=new ModelIndex();
		index.name="idx_uniq";
		index.type=ModelIndex.TYPE_唯一索引;
		index.columnList.add("id");
		model.indexList.add(index);
		EntityCreateTable createTable=new EntityCreateTable(model);
		Object result=dao.createTable(createTable);
		System.out.println("result:"+result);
	}
}
