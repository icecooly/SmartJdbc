package test.domain.query;

import java.util.List;

import io.itit.smartjdbc.Query;
import io.itit.smartjdbc.annotations.InnerJoin;
import io.itit.smartjdbc.annotations.QueryField;
import io.itit.smartjdbc.enums.SqlOperator;
import test.domain.entity.User;

/**
 * 
 * @author skydu
 *
 */
public class UserQuery extends Query<User>{

	@QueryField(operator = SqlOperator.LIKE)
	private String userName;
	
	@QueryField(operator = SqlOperator.LIKE)
	private String name;
	
	@QueryField(operator = SqlOperator.IS_NULL,field = "userName")
	private Boolean userNameIsNull;
	
	@QueryField
	private Integer gender;
	
	@QueryField(field ="age",operator=SqlOperator.GT)
	private Integer gtAge;
	
	@QueryField(field ="age",operator=SqlOperator.LT)
	private Integer ltAge;
	
	@QueryField(field ="status",operator=SqlOperator.IN)
	private List<Integer> statusInList;
	
	@QueryField(field ="status",operator=SqlOperator.NOT_IN)
	private List<Integer> statusNotInList;
	
	@QueryField(field ="status",operator=SqlOperator.IN)
	private Integer[] statusInList2;
	
	
	@QueryField(operator = SqlOperator.JSON_CONTAINS_ANY,field = "roleIdList")
	private Integer roleId; 
	
	@QueryField(operator = SqlOperator.JSON_CONTAINS_ANY)
	private Integer[] roleIdList; 
	
	@QueryField(operator = SqlOperator.JSON_NOT_CONTAINS_ANY,field = "roleIdList")
	private Integer notRoleId; 
	
	@QueryField(operator = SqlOperator.JSON_NOT_CONTAINS_ANY,field = "roleIdList")
	private Integer[] notRoleIdList; 
	
	@InnerJoin(table2 = User.class,table2Alias = "b1",table1Fields ={"createUserId"},table2Fields ={"id"})
	@QueryField(field = "name")
	private String createUserName;
	
	@QueryField(foreignKeyFields = "departmentId",field = "name")
	private String departmentName;
	
	@QueryField(foreignKeyFields = "departmentId",field = "name",operator = SqlOperator.LIKE)
	private String likeDepartmentName;
	
	@QueryField(field = "height",operator = SqlOperator.GE)
	private Double heightStart;
	
	@QueryField(field = "height",operator = SqlOperator.LE)
	private Double heightEnd;
	
	@QueryField
	private Boolean isStudent;
	
	@QueryField
	private Long no;
	
	@QueryField(whereSql = "a.setting is null")
	public Boolean settingIsNull;
	
	@QueryField(operator = SqlOperator.ARRAY_ANY)
	public List<Integer> intArray;
	
	@QueryField(operator = SqlOperator.ARRAY_ANY)
	public List<String> stringArray;
	
	@QueryField(operator = SqlOperator.ARRAY_ANY,field = "stringArray")
	public String[] stringContains;
	
	@QueryField(operator = SqlOperator.ARRAY_NOT_ANY,field = "stringArray")
	public String[] stringNotContains;
	//

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the userNameIsNull
	 */
	public Boolean getUserNameIsNull() {
		return userNameIsNull;
	}

	/**
	 * @param userNameIsNull the userNameIsNull to set
	 */
	public void setUserNameIsNull(Boolean userNameIsNull) {
		this.userNameIsNull = userNameIsNull;
	}

	/**
	 * @return the gender
	 */
	public Integer getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(Integer gender) {
		this.gender = gender;
	}

	/**
	 * @return the gtAge
	 */
	public Integer getGtAge() {
		return gtAge;
	}

	/**
	 * @param gtAge the gtAge to set
	 */
	public void setGtAge(Integer gtAge) {
		this.gtAge = gtAge;
	}

	/**
	 * @return the ltAge
	 */
	public Integer getLtAge() {
		return ltAge;
	}

	/**
	 * @param ltAge the ltAge to set
	 */
	public void setLtAge(Integer ltAge) {
		this.ltAge = ltAge;
	}

	/**
	 * @return the statusInList
	 */
	public List<Integer> getStatusInList() {
		return statusInList;
	}

	/**
	 * @param statusInList the statusInList to set
	 */
	public void setStatusInList(List<Integer> statusInList) {
		this.statusInList = statusInList;
	}

	/**
	 * @return the statusNotInList
	 */
	public List<Integer> getStatusNotInList() {
		return statusNotInList;
	}

	/**
	 * @param statusNotInList the statusNotInList to set
	 */
	public void setStatusNotInList(List<Integer> statusNotInList) {
		this.statusNotInList = statusNotInList;
	}

	/**
	 * @return the statusInList2
	 */
	public Integer[] getStatusInList2() {
		return statusInList2;
	}

	/**
	 * @param statusInList2 the statusInList2 to set
	 */
	public void setStatusInList2(Integer[] statusInList2) {
		this.statusInList2 = statusInList2;
	}

	/**
	 * @return the roleId
	 */
	public Integer getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	/**
	 * @return the roleIdList
	 */
	public Integer[] getRoleIdList() {
		return roleIdList;
	}

	/**
	 * @param roleIdList the roleIdList to set
	 */
	public void setRoleIdList(Integer[] roleIdList) {
		this.roleIdList = roleIdList;
	}

	/**
	 * @return the notRoleId
	 */
	public Integer getNotRoleId() {
		return notRoleId;
	}

	/**
	 * @param notRoleId the notRoleId to set
	 */
	public void setNotRoleId(Integer notRoleId) {
		this.notRoleId = notRoleId;
	}

	/**
	 * @return the notRoleIdList
	 */
	public Integer[] getNotRoleIdList() {
		return notRoleIdList;
	}

	/**
	 * @param notRoleIdList the notRoleIdList to set
	 */
	public void setNotRoleIdList(Integer[] notRoleIdList) {
		this.notRoleIdList = notRoleIdList;
	}

	/**
	 * @return the createUserName
	 */
	public String getCreateUserName() {
		return createUserName;
	}

	/**
	 * @param createUserName the createUserName to set
	 */
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	/**
	 * @return the departmentName
	 */
	public String getDepartmentName() {
		return departmentName;
	}

	/**
	 * @param departmentName the departmentName to set
	 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	/**
	 * @return the likeDepartmentName
	 */
	public String getLikeDepartmentName() {
		return likeDepartmentName;
	}

	/**
	 * @param likeDepartmentName the likeDepartmentName to set
	 */
	public void setLikeDepartmentName(String likeDepartmentName) {
		this.likeDepartmentName = likeDepartmentName;
	}

	/**
	 * @return the heightStart
	 */
	public Double getHeightStart() {
		return heightStart;
	}

	/**
	 * @param heightStart the heightStart to set
	 */
	public void setHeightStart(Double heightStart) {
		this.heightStart = heightStart;
	}

	/**
	 * @return the heightEnd
	 */
	public Double getHeightEnd() {
		return heightEnd;
	}

	/**
	 * @param heightEnd the heightEnd to set
	 */
	public void setHeightEnd(Double heightEnd) {
		this.heightEnd = heightEnd;
	}

	/**
	 * @return the isStudent
	 */
	public Boolean getIsStudent() {
		return isStudent;
	}

	/**
	 * @param isStudent the isStudent to set
	 */
	public void setIsStudent(Boolean isStudent) {
		this.isStudent = isStudent;
	}

	/**
	 * @return the no
	 */
	public Long getNo() {
		return no;
	}

	/**
	 * @param no the no to set
	 */
	public void setNo(Long no) {
		this.no = no;
	}

	/**
	 * @return the settingIsNull
	 */
	public Boolean getSettingIsNull() {
		return settingIsNull;
	}

	/**
	 * @param settingIsNull the settingIsNull to set
	 */
	public void setSettingIsNull(Boolean settingIsNull) {
		this.settingIsNull = settingIsNull;
	}

	/**
	 * @return the intArray
	 */
	public List<Integer> getIntArray() {
		return intArray;
	}

	/**
	 * @param intArray the intArray to set
	 */
	public void setIntArray(List<Integer> intArray) {
		this.intArray = intArray;
	}

	/**
	 * @return the stringArray
	 */
	public List<String> getStringArray() {
		return stringArray;
	}

	/**
	 * @param stringArray the stringArray to set
	 */
	public void setStringArray(List<String> stringArray) {
		this.stringArray = stringArray;
	}

	/**
	 * @return the stringContains
	 */
	public String[] getStringContains() {
		return stringContains;
	}

	/**
	 * @param stringContains the stringContains to set
	 */
	public void setStringContains(String[] stringContains) {
		this.stringContains = stringContains;
	}

	/**
	 * @return the stringNotContains
	 */
	public String[] getStringNotContains() {
		return stringNotContains;
	}

	/**
	 * @param stringNotContains the stringNotContains to set
	 */
	public void setStringNotContains(String[] stringNotContains) {
		this.stringNotContains = stringNotContains;
	}
	
}
