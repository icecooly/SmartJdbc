package test.domain.query;

import java.util.List;

import io.itit.smartjdbc.Query;
import io.itit.smartjdbc.annotations.InnerJoin;
import io.itit.smartjdbc.annotations.Joins;
import io.itit.smartjdbc.annotations.QueryConditionType;
import io.itit.smartjdbc.annotations.QueryField;
import io.itit.smartjdbc.enums.ConditionType;
import io.itit.smartjdbc.enums.SqlOperator;
import test.domain.entity.Article;
import test.domain.entity.ArticleUserLike;
import test.domain.entity.Department;
import test.domain.entity.User;

/**
 * 
 * @author skydu
 *
 */
public class ArticleQuery extends Query<Article>{

	@QueryField(field = "id", operator = SqlOperator.IN)
	private List<Integer> idInList;
	
	@QueryField
	private String title;
	
	@QueryField
	private Integer status;
	
	@QueryField(whereSql=" (title like concat('%',#{titleOrContent},'%') or content like concat('%','${titleOrContent}','%'))")
	private String titleOrContent;
	
	@InnerJoin(table2=User.class,table1Fields= {"createUserId"},table2Fields = {"id"})
	@QueryField(field="name")
	private String createUserName;

	@QueryField(field="mobileNo",foreignKeyFields = "createUserId")
	private String createUserMobileNo;

	@InnerJoin(table2=User.class,table1Fields= {"updateUserId"},table2Fields= {"id"})
	@QueryField(field="name")
	private String updateUserName;
	
	@QueryField(field="status")
	private int[] statusList;
	
	/**likeUserId喜爱的文章*/
	@InnerJoin(table2=ArticleUserLike.class,table1Fields = {"id"},table2Fields= {"articleId"})
	@QueryField(field="userId")
	private Integer likeUserId;
	
	@QueryField(field="name",operator = SqlOperator.LIKE,foreignKeyFields="createUserId,departmentId")
	private String createUserDepartmentName;
	
	@Joins(joins = {
			@InnerJoin(table2 = User.class,table1Fields = {"createUserId"},table2Fields = {"id"}),
			@InnerJoin(table2 = Department.class,table1Fields = {"departmentId"},table2Fields = {"id"})
			})
	@QueryField(field="name",operator = SqlOperator.LIKE_RIGHT)
	private String createUserDepartmentName2;
	
	//
	@QueryConditionType(ConditionType.OR)
	private IdListOrTitle idListOrTitle;
	//
	public static class IdListOrTitle {
		//
		@QueryField(operator = SqlOperator.LIKE)
		private String title;
		
		@QueryField(field = "id", operator = SqlOperator.IN)
		private List<Integer> idInList;
		//

		/**
		 * @return the title
		 */
		public String getTitle() {
			return title;
		}

		/**
		 * @param title the title to set
		 */
		public void setTitle(String title) {
			this.title = title;
		}

		/**
		 * @return the idInList
		 */
		public List<Integer> getIdInList() {
			return idInList;
		}

		/**
		 * @param idInList the idInList to set
		 */
		public void setIdInList(List<Integer> idInList) {
			this.idInList = idInList;
		}
		

	}
	/**
	 * @return the idInList
	 */
	public List<Integer> getIdInList() {
		return idInList;
	}
	/**
	 * @param idInList the idInList to set
	 */
	public void setIdInList(List<Integer> idInList) {
		this.idInList = idInList;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * @return the titleOrContent
	 */
	public String getTitleOrContent() {
		return titleOrContent;
	}
	/**
	 * @param titleOrContent the titleOrContent to set
	 */
	public void setTitleOrContent(String titleOrContent) {
		this.titleOrContent = titleOrContent;
	}
	/**
	 * @return the createUserName
	 */
	public String getCreateUserName() {
		return createUserName;
	}
	/**
	 * @param createUserName the createUserName to set
	 */
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	/**
	 * @return the createUserMobileNo
	 */
	public String getCreateUserMobileNo() {
		return createUserMobileNo;
	}
	/**
	 * @param createUserMobileNo the createUserMobileNo to set
	 */
	public void setCreateUserMobileNo(String createUserMobileNo) {
		this.createUserMobileNo = createUserMobileNo;
	}
	/**
	 * @return the updateUserName
	 */
	public String getUpdateUserName() {
		return updateUserName;
	}
	/**
	 * @param updateUserName the updateUserName to set
	 */
	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}
	/**
	 * @return the statusList
	 */
	public int[] getStatusList() {
		return statusList;
	}
	/**
	 * @param statusList the statusList to set
	 */
	public void setStatusList(int[] statusList) {
		this.statusList = statusList;
	}
	/**
	 * @return the likeUserId
	 */
	public Integer getLikeUserId() {
		return likeUserId;
	}
	/**
	 * @param likeUserId the likeUserId to set
	 */
	public void setLikeUserId(Integer likeUserId) {
		this.likeUserId = likeUserId;
	}
	/**
	 * @return the createUserDepartmentName
	 */
	public String getCreateUserDepartmentName() {
		return createUserDepartmentName;
	}
	/**
	 * @param createUserDepartmentName the createUserDepartmentName to set
	 */
	public void setCreateUserDepartmentName(String createUserDepartmentName) {
		this.createUserDepartmentName = createUserDepartmentName;
	}
	/**
	 * @return the createUserDepartmentName2
	 */
	public String getCreateUserDepartmentName2() {
		return createUserDepartmentName2;
	}
	/**
	 * @param createUserDepartmentName2 the createUserDepartmentName2 to set
	 */
	public void setCreateUserDepartmentName2(String createUserDepartmentName2) {
		this.createUserDepartmentName2 = createUserDepartmentName2;
	}
	/**
	 * @return the idListOrTitle
	 */
	public IdListOrTitle getIdListOrTitle() {
		return idListOrTitle;
	}
	/**
	 * @param idListOrTitle the idListOrTitle to set
	 */
	public void setIdListOrTitle(IdListOrTitle idListOrTitle) {
		this.idListOrTitle = idListOrTitle;
	};
	//
	
}
