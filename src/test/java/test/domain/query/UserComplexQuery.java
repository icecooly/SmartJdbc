package test.domain.query;

import io.itit.smartjdbc.annotations.QueryConditionType;
import io.itit.smartjdbc.annotations.QueryField;
import io.itit.smartjdbc.enums.ConditionType;
import io.itit.smartjdbc.enums.SqlOperator;

/**
 * 
 * @author skydu
 *
 */
public class UserComplexQuery extends UserQuery{
	//
	public static class StatusAndMobile{
		@QueryField
		private Integer status;
		
		@QueryField(operator = SqlOperator.LIKE)
		private String mobileNo;
		//

		/**
		 * @return the status
		 */
		public Integer getStatus() {
			return status;
		}

		/**
		 * @param status the status to set
		 */
		public void setStatus(Integer status) {
			this.status = status;
		}

		/**
		 * @return the mobileNo
		 */
		public String getMobileNo() {
			return mobileNo;
		}

		/**
		 * @param mobileNo the mobileNo to set
		 */
		public void setMobileNo(String mobileNo) {
			this.mobileNo = mobileNo;
		}
		
	}
	//
	public static class NameOrUserNameOrDeptName{
		@QueryField
		private String name;
		
		@QueryField
		private String userName;
		
		@QueryField(foreignKeyFields = "departmentId",field = "name")
		private String deptName;
		
		@QueryConditionType(ConditionType.AND)
		private StatusAndMobile statusAndMobile;
		//

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * @return the userName
		 */
		public String getUserName() {
			return userName;
		}

		/**
		 * @param userName the userName to set
		 */
		public void setUserName(String userName) {
			this.userName = userName;
		}

		/**
		 * @return the deptName
		 */
		public String getDeptName() {
			return deptName;
		}

		/**
		 * @param deptName the deptName to set
		 */
		public void setDeptName(String deptName) {
			this.deptName = deptName;
		}

		/**
		 * @return the statusAndMobile
		 */
		public StatusAndMobile getStatusAndMobile() {
			return statusAndMobile;
		}

		/**
		 * @param statusAndMobile the statusAndMobile to set
		 */
		public void setStatusAndMobile(StatusAndMobile statusAndMobile) {
			this.statusAndMobile = statusAndMobile;
		}
		
	};
	
	@QueryConditionType(ConditionType.OR)
	private NameOrUserNameOrDeptName nameOrUserName;
	
	
	//
	@QueryField(operator = SqlOperator.IS_NOT_NULL)
	private Boolean nameIsNotNull;
	//


	/**
	 * @return the nameOrUserName
	 */
	public NameOrUserNameOrDeptName getNameOrUserName() {
		return nameOrUserName;
	}


	/**
	 * @param nameOrUserName the nameOrUserName to set
	 */
	public void setNameOrUserName(NameOrUserNameOrDeptName nameOrUserName) {
		this.nameOrUserName = nameOrUserName;
	}


	/**
	 * @return the nameIsNotNull
	 */
	public Boolean getNameIsNotNull() {
		return nameIsNotNull;
	}


	/**
	 * @param nameIsNotNull the nameIsNotNull to set
	 */
	public void setNameIsNotNull(Boolean nameIsNotNull) {
		this.nameIsNotNull = nameIsNotNull;
	}
	
}
