package test.domain.vo;

import io.itit.smartjdbc.annotations.Entity;
import io.itit.smartjdbc.annotations.EntityField;

/**
 * 
 * @author skydu
 *
 */
@Entity(tableName="t_user", name = "UserStat")
public class UserStat {

	private Integer gender;

	@EntityField(statFunc="count",field="id")
	private Integer num;
}