package test.domain.entity;

import java.util.Date;
import java.util.List;

import io.itit.smartjdbc.annotations.Entity;
import io.itit.smartjdbc.annotations.EntityField;
import io.itit.smartjdbc.annotations.ForeignKey;
import io.itit.smartjdbc.annotations.PrimaryKey;

/**
 * 文章
 * @author skydu
 *
 */
@Entity(tableName = "t_article", name = "文章")
public class Article{
	//
	public static final int STATUS_待审核=1;
	public static final int STATUS_审核通过=2;
	public static final int STATUS_审核未通过=3;
	
	@PrimaryKey
	private Integer id;
	//
	/**标题*/
	private String title;
	
	/**内容*/
	private String content;
	
	/**状态*/
	private Integer status;

	@ForeignKey(entityClass = User.class)
	private Integer createUserId;
	
	@ForeignKey(entityClass = User.class)
	private Integer updateUserId;
	
	/**创建人名称*/
	@EntityField(foreignKeyFields="createUserId",field="name",persistent = false)
	private String createUserName;
	
	@EntityField(foreignKeyFields="createUserId",field="mobileNo",persistent = false)
	private String createUserMobileNo;
	
	/**创建人所在部门名称*/
	@EntityField(foreignKeyFields="createUserId,departmentId",field="name",persistent = false)
	private String createUserDepartmentName;
	
	/***/
	@EntityField(foreignKeyFields="updateUserId",persistent = false)
	private User updateUser;
	
	private List<User> favoriteUserList;
	
	private Date createTime;

	private Date updateTime;
	
	//
	public Article() {
		
	}
	//
	public Article(String title) {
		this.title=title;
	}
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * @return the createUserId
	 */
	public Integer getCreateUserId() {
		return createUserId;
	}
	/**
	 * @param createUserId the createUserId to set
	 */
	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}
	/**
	 * @return the updateUserId
	 */
	public Integer getUpdateUserId() {
		return updateUserId;
	}
	/**
	 * @param updateUserId the updateUserId to set
	 */
	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}
	/**
	 * @return the createUserName
	 */
	public String getCreateUserName() {
		return createUserName;
	}
	/**
	 * @param createUserName the createUserName to set
	 */
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	/**
	 * @return the createUserMobileNo
	 */
	public String getCreateUserMobileNo() {
		return createUserMobileNo;
	}
	/**
	 * @param createUserMobileNo the createUserMobileNo to set
	 */
	public void setCreateUserMobileNo(String createUserMobileNo) {
		this.createUserMobileNo = createUserMobileNo;
	}
	/**
	 * @return the createUserDepartmentName
	 */
	public String getCreateUserDepartmentName() {
		return createUserDepartmentName;
	}
	/**
	 * @param createUserDepartmentName the createUserDepartmentName to set
	 */
	public void setCreateUserDepartmentName(String createUserDepartmentName) {
		this.createUserDepartmentName = createUserDepartmentName;
	}
	/**
	 * @return the updateUser
	 */
	public User getUpdateUser() {
		return updateUser;
	}
	/**
	 * @param updateUser the updateUser to set
	 */
	public void setUpdateUser(User updateUser) {
		this.updateUser = updateUser;
	}
	/**
	 * @return the favoriteUserList
	 */
	public List<User> getFavoriteUserList() {
		return favoriteUserList;
	}
	/**
	 * @param favoriteUserList the favoriteUserList to set
	 */
	public void setFavoriteUserList(List<User> favoriteUserList) {
		this.favoriteUserList = favoriteUserList;
	}
	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * @return the updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * @param updateTime the updateTime to set
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
}
