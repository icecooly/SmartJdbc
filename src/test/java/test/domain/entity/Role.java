package test.domain.entity;

import java.util.Date;

import io.itit.smartjdbc.annotations.Entity;
import io.itit.smartjdbc.annotations.PrimaryKey;

/**
 * 
 * @author skydu
 *
 */
@Entity(tableName = "t_role", name = "角色")
public class Role {

	@PrimaryKey
	private Integer id;

	/** 角色名称 */
	private String name;

	private Date createTime;

	private Date updateTime;
}
