package test.domain.entity;

import java.util.Date;
import java.util.List;

import io.itit.smartjdbc.annotations.Entity;
import io.itit.smartjdbc.annotations.EntityField;
import io.itit.smartjdbc.annotations.ForeignKey;
import io.itit.smartjdbc.annotations.PrimaryKey;
import io.itit.smartjdbc.enums.ColumnType;
/**
 *用户
 * @author skydu
 *
 */
@Entity(tableName = "t_user", name = "用户")
public class User{
	//
	public static final short GENDER_男=1;
	public static final short GENDER_女=2;
	
	public static final short STATUS_在职=1;
	public static final short STATUS_离职=2;

	@PrimaryKey
	private Integer id;
	
	/**用户名*/
	private String userName;
	
	/**姓名*/
	private String name;
	
	/**手机号*/
	private String mobileNo;
	
	/**性别*/
	private Short gender;
	
	/**工作状态*/
	private Short status;
	
	/**年龄*/
	private Integer age;
	
	/**所属部门*/
	@ForeignKey(entityClass=Department.class)
	private Integer departmentId;
	
	/**角色列表*/
	@EntityField(columnType = ColumnType.JSONB)
	private List<Integer> roleIdList;
	
	/**最后登录时间*/
	private Date lastLoginTime;
	
	/**分表文章数*/
	private Long articleNum;
	
	/**创建人*/
	@ForeignKey(entityClass=User.class)
	private Integer createUserId;
	
	/**最后更新人*/
	@ForeignKey(entityClass=User.class)
	private Integer updateUserId;
	
	/**所属部门名称*/
	@EntityField(foreignKeyFields="departmentId",field="name",persistent = false)
	private String departmentName;
	
	private Double height;
	
	private Long no;
	
	private Boolean isStudent;
	
	private String setting;
	
	private Short[] shortArray;
	
	private Integer[] intArray;

	private Long[] longArray;

	private Float[] floatArray;

	private String[] stringArray;
	
	private Date createTime;
	
	private Date updateTime;
	//

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the mobileNo
	 */
	public String getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo the mobileNo to set
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return the gender
	 */
	public Short getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(Short gender) {
		this.gender = gender;
	}

	/**
	 * @return the status
	 */
	public Short getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Short status) {
		this.status = status;
	}

	/**
	 * @return the age
	 */
	public Integer getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(Integer age) {
		this.age = age;
	}

	/**
	 * @return the departmentId
	 */
	public Integer getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId the departmentId to set
	 */
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return the roleIdList
	 */
	public List<Integer> getRoleIdList() {
		return roleIdList;
	}

	/**
	 * @param roleIdList the roleIdList to set
	 */
	public void setRoleIdList(List<Integer> roleIdList) {
		this.roleIdList = roleIdList;
	}

	/**
	 * @return the lastLoginTime
	 */
	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	/**
	 * @param lastLoginTime the lastLoginTime to set
	 */
	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	/**
	 * @return the articleNum
	 */
	public Long getArticleNum() {
		return articleNum;
	}

	/**
	 * @param articleNum the articleNum to set
	 */
	public void setArticleNum(Long articleNum) {
		this.articleNum = articleNum;
	}

	/**
	 * @return the createUserId
	 */
	public Integer getCreateUserId() {
		return createUserId;
	}

	/**
	 * @param createUserId the createUserId to set
	 */
	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	/**
	 * @return the updateUserId
	 */
	public Integer getUpdateUserId() {
		return updateUserId;
	}

	/**
	 * @param updateUserId the updateUserId to set
	 */
	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	/**
	 * @return the departmentName
	 */
	public String getDepartmentName() {
		return departmentName;
	}

	/**
	 * @param departmentName the departmentName to set
	 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	/**
	 * @return the height
	 */
	public Double getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(Double height) {
		this.height = height;
	}

	/**
	 * @return the no
	 */
	public Long getNo() {
		return no;
	}

	/**
	 * @param no the no to set
	 */
	public void setNo(Long no) {
		this.no = no;
	}

	/**
	 * @return the isStudent
	 */
	public Boolean getIsStudent() {
		return isStudent;
	}

	/**
	 * @param isStudent the isStudent to set
	 */
	public void setIsStudent(Boolean isStudent) {
		this.isStudent = isStudent;
	}

	/**
	 * @return the setting
	 */
	public String getSetting() {
		return setting;
	}

	/**
	 * @param setting the setting to set
	 */
	public void setSetting(String setting) {
		this.setting = setting;
	}

	/**
	 * @return the shortArray
	 */
	public Short[] getShortArray() {
		return shortArray;
	}

	/**
	 * @param shortArray the shortArray to set
	 */
	public void setShortArray(Short[] shortArray) {
		this.shortArray = shortArray;
	}

	/**
	 * @return the intArray
	 */
	public Integer[] getIntArray() {
		return intArray;
	}

	/**
	 * @param intArray the intArray to set
	 */
	public void setIntArray(Integer[] intArray) {
		this.intArray = intArray;
	}

	/**
	 * @return the longArray
	 */
	public Long[] getLongArray() {
		return longArray;
	}

	/**
	 * @param longArray the longArray to set
	 */
	public void setLongArray(Long[] longArray) {
		this.longArray = longArray;
	}

	/**
	 * @return the floatArray
	 */
	public Float[] getFloatArray() {
		return floatArray;
	}

	/**
	 * @param floatArray the floatArray to set
	 */
	public void setFloatArray(Float[] floatArray) {
		this.floatArray = floatArray;
	}

	/**
	 * @return the stringArray
	 */
	public String[] getStringArray() {
		return stringArray;
	}

	/**
	 * @param stringArray the stringArray to set
	 */
	public void setStringArray(String[] stringArray) {
		this.stringArray = stringArray;
	}

	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @param updateTime the updateTime to set
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
}