package io.itit.smartjdbc.provider;

import io.itit.smartjdbc.SmartDataSource;
import io.itit.smartjdbc.provider.entity.EntityCreateTable;

/**
 * 
 * @author skydu
 *
 */
public abstract class CreateTableProvider extends SqlProvider{
	//
	public CreateTableProvider(SmartDataSource smartDataSource) {
		super(smartDataSource);
	}
	//
	protected EntityCreateTable createTable;
	//
	public CreateTableProvider createTable(EntityCreateTable createTable) {
		this.createTable=createTable;
		return this;
	}
}
