package io.itit.smartjdbc.provider.factory;

import io.itit.smartjdbc.SmartDataSource;
import io.itit.smartjdbc.enums.DatabaseType;
import io.itit.smartjdbc.provider.CreateTableProvider;
import io.itit.smartjdbc.provider.impl.pgsql.PgsqlCreateTableProvider;

/**
 * 
 * @author skydu
 *
 */
public class CreateTableProviderFactory {
	//
	public static CreateTableProvider createTable(SmartDataSource smartDataSource) {
		DatabaseType type=smartDataSource.getDatabaseType();
		if(type.equals(DatabaseType.POSTGRESQL)) {
			return new PgsqlCreateTableProvider(smartDataSource);
		}
		throw new RuntimeException("unspoort database type "+type);
	}
}
