package io.itit.smartjdbc.provider.impl.pgsql;

import io.itit.smartjdbc.SmartDataSource;
import io.itit.smartjdbc.provider.DeleteProvider;
import io.itit.smartjdbc.provider.entity.SqlBean;
import io.itit.smartjdbc.provider.where.QueryWhere;
import io.itit.smartjdbc.provider.where.QueryWhere.WhereStatment;
import io.itit.smartjdbc.provider.where.QueryWhereBuilder;
import io.itit.smartjdbc.provider.where.WhereSqlBuilder;

/**
 * 
 * @author skydu
 *
 */
public class PgsqlDeleteProvider extends DeleteProvider{
	//
	public PgsqlDeleteProvider(SmartDataSource smartDataSource) {
		super(smartDataSource);
	}
	//
	@Override
	public SqlBean build() {
		StringBuilder sql=new StringBuilder();
		String tableName=addIdentifierDelimiters(delete.getTableName());
		sql.append("delete from ").append(tableName).append(" ").append(MAIN_TABLE_ALIAS).append(" ");
		WhereStatment ws=null;
		if(queryWhere!=null) {
			ws=new WhereSqlBuilder(getDatabaseType(),queryWhere).build();
			sql.append(ws.sql);
		}
		if(query!=null) {
			QueryWhere qw=QueryWhere.create();
			QueryWhereBuilder qwBuilder=new QueryWhereBuilder(qw, query, entityClass, smartDataSource);
			qwBuilder.build();
			ws=new WhereSqlBuilder(getDatabaseType(),qw).build();
			sql.append(ws.sql);
		}
		if(ws!=null) {
			return SqlBean.build(sql.toString(),ws.values);
		}else {
			return SqlBean.build(sql.toString());
		}
	}
}
