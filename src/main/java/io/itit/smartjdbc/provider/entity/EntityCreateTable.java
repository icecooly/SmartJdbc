package io.itit.smartjdbc.provider.entity;

import io.itit.smartjdbc.model.Model;

/**
 * 
 * @author skydu
 *
 */
public class EntityCreateTable extends Entity{
	//
	public Model model;
	//
	public EntityCreateTable(Model model) {
		super(model.tableName);
		this.model=model;
	}
}
