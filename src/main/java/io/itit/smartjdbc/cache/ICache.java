package io.itit.smartjdbc.cache;

import io.itit.smartjdbc.domain.EntityInfo;

/**
 * 
 * 
 *
 */
public interface ICache {

	EntityInfo getEntityInfo(Class<?> entityClass);
}
