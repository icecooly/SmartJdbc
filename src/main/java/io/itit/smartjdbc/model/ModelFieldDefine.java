package io.itit.smartjdbc.model;

import java.util.List;

import io.itit.smartjdbc.enums.ColumnType;

public class ModelFieldDefine {

	public String name;//companyName ModelDefine下是唯一的
	
	public String realName;//空就是name
	
	public List<String> foreignKeyFields;
	
	public boolean persistent;
	
	public ColumnType columnType;
}
