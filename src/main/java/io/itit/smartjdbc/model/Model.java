package io.itit.smartjdbc.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
public class Model {

	/**
	 * table name
	 */
	public String tableName;
	
	/**
	 * 
	 */
	public String schema;
	
	/**
	 * 
	 */
	public String comment;
	
	public List<ModelColumn> columnList=new ArrayList<>();
	
	public ModelPrimaryKey primaryKey;
	
	public List<ModelIndex> indexList;
	
}
