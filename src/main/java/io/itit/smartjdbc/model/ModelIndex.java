package io.itit.smartjdbc.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
public class ModelIndex {

	public static final String TYPE_普通索引="normal";
	public static final String TYPE_唯一索引="unique";
	
	public String name;
	
	public String type;
	
	public List<String> columnList=new ArrayList<>();
}
