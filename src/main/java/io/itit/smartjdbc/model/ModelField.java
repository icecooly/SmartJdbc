package io.itit.smartjdbc.model;

public class ModelField {

	public String leftJoinTableAlias;

	public String innerJoinTableAlias;
	
	public ModelFieldDefine field;
	
	public String columnName;
}
