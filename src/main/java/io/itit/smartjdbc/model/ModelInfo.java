package io.itit.smartjdbc.model;

import java.util.List;

import io.itit.smartjdbc.enums.JoinType;

/**
 * 
 */
public class ModelInfo {

	public ModelDefine modelDefine;
	
	public List<ModelJoin> joins;
	
	public List<ModelField> fieldList;
	
	public static class ModelJoin{
		public JoinType joinType;
		public String table1Alias;
		public String table1;
		public String table1Remark;
		public String table2Alias;
		public String table2;
		public String table2Remark;
		public String[] table1Fields;
		public String[] table2Fields;
	}
}
