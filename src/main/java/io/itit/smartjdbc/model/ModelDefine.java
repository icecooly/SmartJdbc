package io.itit.smartjdbc.model;

import java.util.List;

/**
 * 
 */
public class ModelDefine {

	public String tableName;
	
	public List<ModelFieldDefine> fieldList;
}
