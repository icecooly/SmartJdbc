package io.itit.smartjdbc.model;

import io.itit.smartjdbc.enums.ColumnType;

public class ModelColumn {

	public String name;
	
	public ColumnType type;
	
	public int length;
	
	public boolean notnull;
	
	public Object defaultValue;
	
	public String comment;
	
	
}
