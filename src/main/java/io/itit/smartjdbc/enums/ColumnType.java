package io.itit.smartjdbc.enums;


/**
 * 
 * @author skydu
 *
 */
public enum ColumnType {
	NONE,
	INT,
    BIGINT,
    TINYINT,
    SMALLINT,
    MEDIUMINT,
    LONG,
    FLOAT,
    DOUBLE,
    BIGDECIMAL,
    CHAR,
    VARCHAR,
    TEXT,
    MEDIUMTEXT,
    DATE,
    TIMESTAMP,
    JSONB,
    TEXT_ARRAY,
    VARCHAR_ARRAY,
    INT_ARRAY,
    FLOAT_ARRAY,
    BOOL,
    LTREE
}
